﻿using System;
using System.Collections.Generic;

namespace MatrixElementsSum
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] matrix = new int[3][];
            matrix[0] = new int[4]{0, 1, 1, 2};
            matrix[1] = new int[4]{0, 5, 0, 0};
            matrix[2] = new int[4]{2, 0, 3, 3};

            int[][] matrix1 = new int[4][];
            matrix1[0] = new int[1]{1};
            matrix1[1] = new int[1]{5};
            matrix1[2] = new int[1]{0};
            matrix1[3] = new int[1]{2};

            int sum = matrixElementsSum(matrix);
            int sum1 = matrixElementsSum(matrix1);

            System.Console.WriteLine(sum + " " + sum1);


        }

        static int matrixElementsSum(int[][] matrix) {
            
            
            List<int> checker = new List<int>();
            
            
            int sumOfElements = 0;

            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                        if(matrix[i][j] == 0){
                            checker.Add(j);
                        }else{
                            if(!checker.Contains(j)){
                            sumOfElements += matrix[i][j];
                            }else{
                                checker.Add(j);
                            }
                        }
                }

            }

            return sumOfElements;
        }

    }
}
